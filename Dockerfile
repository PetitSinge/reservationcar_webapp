FROM python:3.8-alpine

RUN apk update

COPY /app /app
WORKDIR /app

# Install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["view.py"]