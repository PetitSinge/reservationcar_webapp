from flask import Flask, render_template, request, redirect, url_for

import datetime
import os

app = Flask(__name__)
app.secret_key = 'votre_clé_secrète'  # Changez cette clé pour des raisons de sécurité.

# Liste de réservations, chaque élément est une liste [date, emplacement, foodtruck]
reservations = []

# Dictionnaire d'emplacements
emplacements = {
    'emplacement1': 'Emplacement 1',
    'emplacement2': 'Emplacement 2',
    'emplacement3': 'Emplacement 3',
    'emplacement4': 'Emplacement 4',
    'emplacement5': 'Emplacement 5',
    'emplacement6': 'Emplacement 6',
    'emplacement7': 'Emplacement 7',
}

# Dictionnaire pour suivre les réservations par foodtruck et dates associées
reservations_foodtrucks = {}

# Page d'accueil
@app.route('/')
def index():
    return render_template('index.html')

# Page de réservation
@app.route('/reserver', methods=['GET', 'POST'])
def reserver():
    if request.method == 'POST':
        date = request.form.get('date')
        emplacement = request.form.get('emplacement')
        foodtruck = request.form.get('foodtruck')

        # Vérifie si la date est future
        today = datetime.date.today()
        reservation_date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        if reservation_date < today:
            return "Impossible de réserver pour une date passée."

        # Vérifie si le foodtruck a déjà réservé cette semaine
        if foodtruck in reservations_foodtrucks:
            for reserved_date in reservations_foodtrucks[foodtruck]:
                if reservation_date.isocalendar()[1] == reserved_date.isocalendar()[1]:
                    return "Le foodtruck a déjà réservé une place cette semaine."

        # Ajoute la réservation à la liste
        reservations.append([date, emplacement, foodtruck])

        # Met à jour le dictionnaire des réservations par foodtruck
        if foodtruck in reservations_foodtrucks:
            reservations_foodtrucks[foodtruck].append(reservation_date)
        else:
            reservations_foodtrucks[foodtruck] = [reservation_date]

        return redirect(url_for('liste'))

    return render_template('reservation.html', emplacements=emplacements)

# Page de liste des réservations
@app.route('/liste')
def liste():
    return render_template('liste.html', reservations=reservations, emplacements=emplacements)

# Supprimer une réservation
@app.route('/supprimer/<int:index>')
def supprimer(index):
    if 0 <= index < len(reservations):
        date, emplacement, foodtruck = reservations[index]
        reservations.pop(index)
        if foodtruck in reservations_foodtrucks:
            for reserved_date in reservations_foodtrucks[foodtruck]:
                if reserved_date == date:
                    reservations_foodtrucks[foodtruck].remove(reserved_date)
    return redirect(url_for('liste'))

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5066))
    app.run(debug=True, host='0.0.0.0', port=port)