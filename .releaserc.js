module.exports = {
    "release": {
        "release": {
            "analyzeCommits": {
              "path": "semantic-release-conventional-commits",
              "majorTypes": ["major", "breaking"],
              "minorTypes": ["feat", "minor"],
              "patchTypes": ["fix", "patch"],
              "mergePattern": "/^Merge pull request #(\\d+) from (.*)$/",
              "mergeCorrespondence": "['id', 'source']"
            }
          },
        "plugins": [
         [
          "@semantic-release/commit-analyzer",
          {
           "preset": "conventionalcommits"
          }
         ],
         [
          "@semantic-release/release-notes-generator",
          {
           "preset": "conventionalcommits"
          }
         ],
         [
          "@semantic-release/npm",
          {
           "npmPublish": false
          }
         ],
         [
          "@semantic-release/exec",
          {
           "prepareCmd": "yarn run build"
          }
         ],
         "@semantic-release/changelog",
         [
          "@semantic-release/git",
          {
           "assets": [
            "package.json",
            "changelog"
           ],
           "message": "chore(release): ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
          }
         ],
         [
          "@semantic-release/github",
          {
           "assets": [
            {
             "path": "dist/atomic-calendar-revive.js"
            }
           ]
          }
         ]
        ]
       }
  };    